# Aplikasi Pengiriman Barang Logistik PT. POS INDONESIA BANJARMASIN (Persero)
## Deskripsi
Aplikasi ini mengambil studi kasus pada PT. POST INDONESIA BANJARMASIN, sesuai dengan namanya, aplikasi ini digunakan untuk memantau pengiriman barang logistik dari POS yang satu ke POS tujuan. hal yang dapat ditangani pada aplikasi ini berupa :
1. Verifikasi Barang Logistik sudah diterima.
2. Pembatalan Verifikasi Barang logistik.
3. Hak akses terdiri dari 2 level, yaitu Administrator -> PT POS Banjarmasin, sedangkan User -> cabang dari PT. POS Banjarmasin.
Aplikasi ini jauh dari kata sempurna, apalagi aplikasi ini saya buat pada tahun 2014 yang lalu, dimana saat itu saya masih baru belajar pemrograman PHP dengan Framework CodeIgniter. hal ini dapat dilihat pada spesifikasi & component yang saya gunakan pada project ini.

## Spesifikasi
PHP versi 5.5.6 dan database MySQL

## Componnent
1. CI 2.1.4
2. Template Kickstart (Jadul amat men wkwkwk, tapi kalau diliat-liat di situsnya sekarang terlihat lebih keren... dulu belum kenal bootstrap) (http://getkickstart.com/)
3. DOMPDF
4. jQuery v1.9.1

Akhir kata saya ucapkan terimakasih banyak kepada semua pihak yang telah membagikan Source Code secara gratis agar saya bisa gunakan sebagai komponen aplikasi ini. silahkan klik tautan yang tertera untuk melihat repository milik mereka masing-masing ;)

## Support Me
For Donate : ***BRI (0239 01 023305 50 9) A.n Nasrullah Siddik*** Thanks :).

## Contact
1. Telegram :[@as_shiddiq](http://telegram.me/@as_shiddiq)
2. Facebook : [as.shiddiq](http://fb.com/as.shiddiq)
3. Email : n.shiddiq@gmail.com

## PREVIEW
### Bidang Login
![LOGISTIK POS LOGIN](./asset/screenshoot/login.png)
### Halaman Admin & Menu
![LOGISTIK POS LOGIN](./asset/screenshoot/admin-beranda.png)
![LOGISTIK POS LOGIN](./asset/screenshoot/admin-menu.png)
![LOGISTIK POS LOGIN](./asset/screenshoot/admin-logistik.png)
![LOGISTIK POS LOGIN](./asset/screenshoot/admin-akun.png)
### Halaman Admin & Menu
![LOGISTIK POS LOGIN](./asset/screenshoot/user-menu.png)
![LOGISTIK POS LOGIN](./asset/screenshoot/user-verif.png)
#### Laporan
![LOGISTIK POS LOGIN](./asset/screenshoot/laporan.png)
__nb__: dan untuk lebih jelas silahkan clone/unduh repository ini.
