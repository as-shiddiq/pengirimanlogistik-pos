<?php
class Admin extends CI_controller{
	function __construct(){
		parent::__construct();
		$this->__cek_logged();
	}
	private function __cek_logged(){
		if(!$this->session->userdata('admin')){
            
            redirect(base_url().'home');
        }
       

	}
	function index(){
		$data['title']='Beranda - Admin';
		$data['body']=$this->load->view('v_admin_beranda','',true);
		$this->load->view('v_html',$data);
	}

	function pelanggan($ac="",$id=""){
		$db='m_admin';
		if($ac=="tambah"){
			if ($this->input->post('simpan')){
                     
                     //cek validation inputan
                     
                        $this->form_validation->set_rules('nama_cos','Pengirim','trim|required|xss_clean');
                        $this->form_validation->set_rules('alamat_cos','Alamat','trim|required|xss_clean');
                        $this->form_validation->set_rules('hp_cos','No.Hp / Telp','trim|required|is_numeric|min_length[10]|max_length[15]|xss_clean');
                       
                        $this->form_validation->set_error_delimiters('<div class="warning-valid">','</div>');
                        
                        //jika validasi benar
                        if($this->form_validation->run()==TRUE) {
                       
                        $this->$db->input_pelanggan();

                        //membuat session untuk validasi
                        $info='<div class="success">Sukses Ditambah</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'admin/pelanggan');
                        }            
                     }
                     elseif ($this->input->post('batal')){
                     
                        redirect(base_url().'admin/pelanggan');
                     }

				$sub_data['tampil']='';
				$data['title']='Tambah Pelanggan - Admin';
				$data['body']=$this->load->view('v_input_pelanggan',$sub_data,true);

		}
		elseif($ac=="edit"){
			if($id!=""){
						if ($this->input->post('simpan')){
			                     
			                     //cek validation inputan
			                     
			                        $this->form_validation->set_rules('nama_cos','Pengirim','trim|required|xss_clean');
			                        $this->form_validation->set_rules('alamat_cos','Alamat','trim|required|xss_clean');
			                        $this->form_validation->set_rules('hp_cos','No.Hp / Telp','trim|required|is_numeric|min_length[10]|max_length[15]|xss_clean');
			                       
                        
			                        $this->form_validation->set_error_delimiters('<div class="warning-valid">','</div>');
			                        
			                        //jika validasi benar
			                        if($this->form_validation->run()==TRUE) {
			                       
			                        $this->$db->update_data_pelanggan($id);
			
			                        //membuat session untuk validasi
			                        $info='<div class="success">Sukses Diedit</div>';
			                        $this->session->set_flashdata('info',$info);
			                        //end
			                        redirect(base_url().'admin/pelanggan');
			                        }            
			                     }
			                     elseif ($this->input->post('batal')){
			                     
			                        redirect(base_url().'admin/pelanggan');
			                     }
							$sub_data['tampil']=$this->$db->tampil_edit_pelanggan($id);
							$data['title']='Edit Pelanggan - Admin';
							$data['body']=$this->load->view('v_input_pelanggan',$sub_data,true);
						}
				else{
					redirect('admin/pelanggan');
				}

		}
		elseif($ac=="hapus"){
			if($id!=""){
				$this->$db->hapus_pelanggan($id);
				 //membuat session untuk validasi
                        $info='<div class="success">Sukses Dihapus</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'admin/pelanggan');
			}
		}
		else{	

				$sub_data['info']=$this->session->flashdata('info');
				$sub_data['tampil']=$this->$db->tampil_data_pelanggan();
				$data['title']='Pelanggan - Admin';
				$data['body']=$this->load->view('v_pelanggan',$sub_data,true);
			}

		$this->load->view('v_html',$data);



	}


	function akun($ac="",$id=""){
		$db='m_admin';
		if($ac=='tambah'){
				if ($this->input->post('simpan')){
                     
                     //cek validation inputan
                     
                        $this->form_validation->set_rules('nama_user','Nama Pengguna','trim|required|xss_clean');
                        $this->form_validation->set_rules('password','Password','trim|required|xss_clean');
                        $this->form_validation->set_rules('level','Level','trim|required|xss_clean');
                        $this->form_validation->set_rules('nama_pos','Nama Pos','trim|required|xss_clean');
                        $this->form_validation->set_rules('alamat_pos','Wilayah','trim|required|xss_clean');
                       
                        $this->form_validation->set_error_delimiters('<div class="warning-valid">','</div>');
                        
                        //jika validasi benar
                        if($this->form_validation->run()==TRUE) {
                       
                        $this->$db->input_akun_login();

                        //membuat session untuk validasi
                        $info='<div class="success">Sukses Ditambah</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'admin/akun');
                        }            
                     }
                     elseif ($this->input->post('batal')){
                     
                        redirect(base_url().'admin/akun');
                     }
				$sub_data['tampil']='';
				$data['title']='Tambah Akun - Admin';
				$data['body']=$this->load->view('v_input_akun',$sub_data,true);
		}
		elseif($ac=='edit'){
			if($id!=''){
							if ($this->input->post('simpan')){
			                     
			                     //cek validation inputan
			                     
			                        $this->form_validation->set_rules('password','Pasword','trim|required|xss_clean');
			                        $this->form_validation->set_rules('nama_pos','Nama Pos','trim|required|xss_clean');
			                        $this->form_validation->set_rules('alamat_pos','Alamat Pos','trim|required|xss_clean');
			                       
			                        $this->form_validation->set_error_delimiters('<div class="warning-valid">','</div>');
			                        
			                        //jika validasi benar
			                        if($this->form_validation->run()==TRUE) {
			                       
			                        $this->$db->update_akun_login($id);
			
			                        //membuat session untuk validasi
			                        $info='<div class="success">Edit Sukses</div>';
			                        $this->session->set_flashdata('info',$info);
			                        //end
			                        redirect(base_url().'admin/akun');
			                        }            
			                     }
			                     elseif ($this->input->post('batal')){
			                     
			                        redirect(base_url().'admin/akun');
			                     }
							$sub_data['tampil']=$this->$db->tampil_edit_akun($id);
							$data['title']='Edit Akun - Admin';
							$data['body']=$this->load->view('v_input_akun',$sub_data,true);}
				else{
					redirect('admin/akun');
				}
		}
		elseif($ac=="hapus"){
			if($id!=""){
				$this->$db->hapus_akun($id);
				 //membuat session untuk validasi
                        $info='<div class="success">Sukses Dihapus</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'admin/akun');
			}
		}
		else{	

				$sub_data['info']=$this->session->flashdata('info');
				$sub_data['tampil']=$this->$db->tampil_data_akun();
				$data['title']='Akun - Admin';
				$data['body']=$this->load->view('v_akun',$sub_data,true);
			}

		$this->load->view('v_html',$data);

	}




	function paket($ac="",$id=""){

		$db='m_admin';
		if($ac=='tambah'){
				if ($this->input->post('simpan')){
                     
                     //cek validation inputan
                     
                        $this->form_validation->set_rules('berat_paket','Berat Paket / Kg','trim|required|xss_clean');
                        $this->form_validation->set_rules('jumlah_paket','Jumlah Paket / Buah','trim|required|xss_clean');
                        $this->form_validation->set_error_delimiters('<div class="warning-valid">','</div>');
                        
                        //jika validasi benar
                        if($this->form_validation->run()==TRUE) {
                       
                        $this->$db->input_paket();

                        //membuat session untuk validasi
                        $info='<div class="success">Sukses Ditambah</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'admin/paket');
                        }            
                     }
                     elseif ($this->input->post('batal')){
                     
                        redirect(base_url().'admin/paket');
                     }
                $sub_data['tampil']='';
				$data['title']='Tambah Paket - Admin';
				$data['body']=$this->load->view('v_input_paket',$sub_data,true);
		}

		elseif($ac=='edit'){
			if($id!=''){
				if ($this->input->post('simpan')){
                     
                     //cek validation inputan
                     
                        $this->form_validation->set_rules('berat_paket','Berat Paket / Kg','trim|required|xss_clean');
                        $this->form_validation->set_rules('jumlah_paket','Jumlah Paket / Buah','trim|required|xss_clean');
                        $this->form_validation->set_error_delimiters('<div class="warning-valid">','</div>');
                        
                        //jika validasi benar
                        if($this->form_validation->run()==TRUE) {
                       
                        $this->$db->update_paket($id);

                        //membuat session untuk validasi
                        $info='<div class="success">Sukses Diedit</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'admin/paket');
                        }            
                     }
                     elseif ($this->input->post('batal')){
                     
                        redirect(base_url().'admin/paket');
                     }
                $sub_data['tampil']=$this->$db->tampil_edit_data($id);
				$data['title']='Edit Paket - Admin';
				$data['body']=$this->load->view('v_input_paket',$sub_data,true);
			}
			else{
				redirect('admin/paket');
			}
		}
		elseif($ac=="hapus"){
			if($id!=""){
				$this->$db->hapus_paket($id);
				 //membuat session untuk validasi
                        $info='<div class="success">Sukses Dihapus</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'admin/paket');
			}
		}
		else{	

				$sub_data['info']=$this->session->flashdata('info');
				$sub_data['tampil']=$this->$db->tampil_data();
				$data['title']='Paket - Admin';
				$data['body']=$this->load->view('v_admin_paket',$sub_data,true);
			}

		$this->load->view('v_html',$data);

	}

	function printkonfirm($id=""){
		$db='m_admin';
		if($id!==''){
				$data['tampil']=$this->$db->printkonfirm($id);
				$this->load->view('v_print',$data);
		}

		else{
			redirect(base_url().'admin/paket');
		}
	}

	function laporan($id=""){
		$db='m_admin';
		$sub_data='';
		if ($this->input->post('tampilkan')){
                     
                     //cek validation inputan
                     
                        $this->form_validation->set_rules('mulai_tgl','Mulai Tanggal','trim|required|xss_clean');
                        $this->form_validation->set_rules('sampai_tgl','Sampai Tanggal','trim|required|xss_clean');
                        $this->form_validation->set_error_delimiters('<div class="warning-valid">','</div>');
                        
                        //jika validasi benar
                        if($this->form_validation->run()==TRUE) {
                        	$sub_data['tampil']=$this->$db->tampil_laporan();
                        }            
                     }

		$data['body']=$this->load->view('v_laporan',$sub_data,true); 
		$data['title']='Laporan - Admin';
		$this->load->view('v_html',$data);

	}
	function renderlaporan(){
		if(isset($_GET['mulai']) AND isset($_GET['sampai'])){

			$db='m_admin';
            $sub_data['tampil']=$this->$db->tampil_laporan();
			$pdf=$this->load->view('v_laporan_pdf',$sub_data,true);
		  	generate_pdf($pdf);	
		}

	}
}
