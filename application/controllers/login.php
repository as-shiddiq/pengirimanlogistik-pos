<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function index(){
        
        $sub_data['info']=$this->session->userdata('info');
        $data['title']='Login';

        
        if ($this->input->post('login')){
            
            $this->form_validation->set_rules('nama_user','Nama Pengguna','trim|required|max_length[20]|xss_clean');
            $this->form_validation->set_rules('password','Password','trim|required|max_length[20]|xss_clean');
            $this->form_validation->set_error_delimiters('<div class="warning-valid">','</div>');
               
            
            
        if($this->form_validation->run()==TRUE){
                $this->m_login->proses_login();
            }    
        
        }
    
    $data['body']=$this->load->view('v_login',$sub_data,TRUE);
    $this->load->view('v_tampil_login',$data);   
    

    //unset session
    $this->session->unset_userdata('info');       
        
    }
    
    function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
        
    }

}