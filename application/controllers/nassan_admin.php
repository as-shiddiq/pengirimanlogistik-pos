<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Nassan_admin extends CI_controller {
	function __construct(){
		parent::__construct();
		$this->__cek_logged();
	}
	private function __cek_logged(){
		if($this->session->userdata('admin')){
            
            redirect(base_url().'admin');
        }
        elseif($this->session->userdata('user')){

            redirect(base_url().'user');
        }
        else{

            redirect(base_url().'login/');
        }
       

	}
}
