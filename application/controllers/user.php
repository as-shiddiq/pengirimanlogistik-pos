<?php

class User extends CI_controller{
	function __construct(){
		parent::__construct();
		$this->__cek_logged();
	}
	private function __cek_logged(){
		if(!$this->session->userdata('user')){
            
            redirect(base_url().'home');
        }
    }
       
	function index(){
		$data['title']='Beranda - User';
		$data['body']=$this->load->view('v_admin_beranda','',true);
		$this->load->view('v_html',$data);
	}
	function paket($ac="",$id=""){

		$db='m_user';
		if($ac=="belum"){
		
		$data['title']='Paket Belum Sampai - User';
		$sub_data['info']=$this->session->flashdata('info');
		$sub_data['tampil']=$this->$db->tampil_data_belum();
		$data['body']=$this->load->view('v_user_paket',$sub_data,true);
		$this->load->view('v_html',$data);

		}
		elseif($ac=="konfirmasi"){
			if($id!=""){
				$this->$db->terima_paket($id);

			 //membuat session untuk validasi
                        $info='<div class="success">Sukses Dikonfirmasi</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'user/paket/belum');

			}
		}
		elseif($ac=="batal"){
			if($id!=""){
				$this->$db->batal_paket($id);

			 //membuat session untuk validasi
                        $info='<div class="success">Sukses Dibatalkan</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'user/paket/sudah');

			}
		}
		elseif($ac=="sudah"){
		
		$data['title']='Paket Sudah Sampai - User';
		$sub_data['info']=$this->session->flashdata('info');
		$sub_data['tampil']=$this->$db->tampil_data_sudah();
		$data['body']=$this->load->view('v_user_paket_sudah',$sub_data,true);
		$this->load->view('v_html',$data);

		}
		else {
			redirect(base_url().'user/paket/sudah');
		}

		
	}
}