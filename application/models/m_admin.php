<?php

class M_admin extends CI_model{
	function tampil_data_pelanggan(){
		$q="select * from costumer order by id_cos DESC";
		return $this->db->query($q);
	}
	

	function hapus_pelanggan($id){
		$q="delete from costumer where id_cos='".$id."'";
		return $this->db->query($q);
	}

	function tampil_data_akun(){
		$q="select * from pos order by id_user DESC";
		return $this->db->query($q);
	}

	function hapus_akun($id){
		if($id==1){
			//membuat session untuk validasi
                        $info='<div class="warning">Gagal Dihapus (Admin Dilarang Dihapus)</div>';
                        $this->session->set_flashdata('info',$info);
                        //end
                        redirect(base_url().'admin/akun');
		}
		else{
		$q="delete from pos where id_user='".$id."'";
		return $this->db->query($q);
	}
	}

	function tampil_data(){
		$q="select * from paket JOIN costumer ON paket.id_cos=costumer.id_cos JOIN pos ON paket.id_user=pos.id_user order by paket.id_paket DESC";
		return $this->db->query($q);
	}

	function hapus_paket($id){
		$q="delete from paket where id_paket='".$id."'";
		return $this->db->query($q);
	}

	function input_pelanggan(){
		$nama_cos=$_POST['nama_cos'];
		$alamat_cos=$_POST['alamat_cos'];
		$hp_cos=$_POST['hp_cos'];
		
		$q="select * from costumer where hp_cos='".$hp_cos."'";
		$cek=$this->db->query($q);


		if($cek->num_rows()==0){
		$data=array(
		'id_cos'=>'',
		'nama_cos'=>$nama_cos,
		'alamat_cos'=>$alamat_cos,
		'hp_cos'=>$hp_cos,
		


			);

		return $this->db->insert('costumer',$data);
	}
	else{

			$info='<div class="warning">Gagal Tambah (Data Duplikat)</div>';
                    $this->session->set_flashdata('info',$info);

                    redirect(base_url().'admin/pelanggan');
	}

	}
	function input_akun_login(){
		$nama_user=$_POST['nama_user'];
		$password=$_POST['password'];
		$level=$_POST['level'];
		$nama_pos=$_POST['nama_pos'];
		$alamat_pos=$_POST['alamat_pos'];
		
		$q="select * from pos where nama_user='".$nama_user."' OR nama_pos='".$nama_pos."' OR alamat_pos='".$alamat_pos."'";
		$cek=$this->db->query($q);

		if($cek->num_rows()==0){
				$data=array(
				'id_user'=>'',
				'nama_user'=>$nama_user,
				'password'=>$password,
				'level'=>$level,
				'nama_pos'=>$nama_pos,
				'alamat_pos'=>$alamat_pos
		
		
					);
		
				return $this->db->insert('pos',$data);
		}

		else{
			$info='<div class="warning">Gagal Tambah (Data Duplikat)</div>';
                    $this->session->set_flashdata('info',$info);

                    redirect(base_url().'admin/akun');
		}
	}
	
	function input_paket(){
		$id_cos=$_POST['id_cos'];
		$id_user=$_POST['id_user'];
		$berat_paket=$_POST['berat_paket'];
		$jumlah_paket=$_POST['jumlah_paket'];
		$tgl_kirim=date('Y-m-d');

		$data=array(
		'id_cos'=>$id_cos,
		'id_user'=>$id_user,
		'berat_paket'=>$berat_paket,
		'jumlah_paket'=>$jumlah_paket,
		'tgl_kirim'=>$tgl_kirim


			);

		return $this->db->insert('paket',$data);


	}


	function tampil_edit_pelanggan($id){
		$q="select * from costumer where id_cos='".$id."'";
		return $this->db->query($q);
	}

	function tampil_edit_akun($id){
		$q="select * from pos where id_user='".$id."'";
		return $this->db->query($q);
	}
	function tampil_edit_data($id){
		$q="select * from paket where id_paket='".$id."'";
		return $this->db->query($q);
	}

	function update_data_pelanggan($id){
		$nama_cos=$_POST['nama_cos'];
		$alamat_cos=$_POST['alamat_cos'];
		$hp_cos=$_POST['hp_cos'];
		
		$q="UPDATE costumer SET nama_cos='".$nama_cos."' , alamat_cos='".$alamat_cos."' , hp_cos='".$hp_cos."' where id_cos='".$id."'";

		return $this->db->query($q);
	
}

function update_akun_login($id){
		$password=$_POST['password'];
		$nama_pos=$_POST['nama_pos'];
		$alamat_pos=$_POST['alamat_pos'];
		
		
		$q="UPDATE  `post`.`pos` SET `password` =  '".$password."',
`nama_pos` =  '".$nama_pos."',
`alamat_pos` =  '".$alamat_pos."' WHERE  `pos`.`id_user` =".$id." LIMIT 1 ;";

			
		
				return $this->db->query($q);
		

		
	}


	function update_paket($id){
		$id_cos=$_POST['id_cos'];
		$id_user=$_POST['id_user'];
		$berat_paket=$_POST['berat_paket'];
		$jumlah_paket=$_POST['jumlah_paket'];

		
		$q="UPDATE  `post`.`paket` SET  `id_cos` =  '".$id_cos."',
`id_user` =  '".$id_user."',
`berat_paket` =  '".$berat_paket."',
`jumlah_paket` =  '".$jumlah_paket."' WHERE  `paket`.`id_paket` ='".$id."' LIMIT 1 ;
";
		return $this->db->query($q);


	}

	function printkonfirm($id){
		$q="select * from paket JOIN costumer ON paket.id_cos=costumer.id_cos JOIN pos ON paket.id_user=pos.id_user where paket.id_paket='".$id."'";
		return $this->db->query($q);
	}

	function tampil_laporan(){
		if(isset($_POST['mulai_tgl']) AND isset($_POST['sampai_tgl'])){
			$mulai=$_POST['mulai_tgl'];
			$sampai=$_POST['sampai_tgl'];
		}
		elseif(isset($_GET['mulai']) AND isset($_GET['sampai'])){
			$mulai=$_GET['mulai'];
			$sampai=$_GET['sampai'];
		}
		$q="select * from paket a JOIN costumer b ON a.id_cos=b.id_cos JOIN pos c ON a.id_user=c.id_user WHERE a.tgl_kirim >='".$mulai."' AND a.tgl_kirim<='".$sampai."' order by a.id_paket DESC";
		return $this->db->query($q);
	}
}