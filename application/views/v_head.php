<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asset/css/kickstart.css" media="all" />                  <!-- KICKSTART -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asset/css/style.css" media="all" />                          <!-- CUSTOM STYLES -->
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/kickstart.js"></script>                                  <!-- KICKSTART -->

		
<style>
a {
	text-decoration: none
}
body{
	background-color: #f9f9f9 ;
	background:url(<?php echo base_url().'asset/css/img/header.png'?>) top right no-repeat, url(<?php echo base_url().'asset/css/img/back.png'?>) ;
}
.warning-valid,.warning{
	color:red;
	background: #fee;
	padding:10px;
	margin:10px auto 15px;
	border:1px solid #900;
}
table{
	padding:5px;
	margin:10px 0;
	border:1px solid #f0f0f0;
}
table th{
	background:#f90;
	color:#fefefe;
	padding: 10px
}
table tr:nth-child(odd) td{
		background: #f0f0f0
	}
table tr td {
	border-right:1px solid #eee;
}
table tr td:last-child {
	text-align: center
}

#footer{
	padding:20px 0;
	border-top:1px dashed #f0f0f0;
	margin:10px 0 0 10px;
}

.success{
	color:green;
	background: #efe;
	padding:10px;
	margin:10px auto 15px;
	border:1px solid #090;
}

/**MENU**/
.menuu{
	background:url(<?php echo base_url().'asset/css/img/menu.png'?>) center center repeat-x !important;
	height: 50px;
	position: fixed;
	width: 100%;
	border-bottom: 1px solid #333;
	z-index: 1000


}
.menu {
text-align: right;
	background:transparent;!important;
	right:0;
	border:0;
	transition:.3s all;
	-webkit-transition:.3s all;
	-moz-transition:.3s all;
	-o-transition:.3s all;
}

.menu li a{
	color:#999;
	text-shadow:none;

}
#current-menu {
	border-bottom: 5px solid #f90;
}



.menu li:hover{
	border-bottom: 5px solid #f90;
	
	transition:.3s all;
	-webkit-transition:.3s all;
	-moz-transition:.3s all;
	-o-transition:.3s all;

}
.menu li:hover a{
	background: transparent !important;
	color:#f90;
	transition:.3s all;
	-webkit-transition:.3s all;
	-moz-transition:.3s all;
	-o-transition:.3s all;

}

.menu li:hover ul li a{
	background: transparent !important;
	color:#999;
	transition:.3s all;
	-webkit-transition:.3s all;
	-moz-transition:.3s all;
	-o-transition:.3s all;

}

.menu li ul li {
	text-align: left;

}
.menu li ul{
	background: #222 !important;
	border: 0
}
.menu li:hover ul li{
	border-bottom: none;
	transition:.3s all;
	-webkit-transition:.3s all;
	-moz-transition:.3s all;
	-o-transition:.3s all;

}

.menu li ul li:hover a{
	background: transparent !important;
	color:#f90;
	transition:.3s all;
	-webkit-transition:.3s all;
	-moz-transition:.3s all;
	-o-transition:.3s all;

}

.menu li ul li:hover  {
	border-bottom: none;
	background: #333;

}
.menuu h5{
	color:#999 !important;
	display: inline-block;
	font-size: 15px;
	float:left;
	margin: 0;
	padding: 10px 10px 10px 40px

}

/**HEADER**/

#header{
	margin-top: 50px;
}

#header-content{
	height:248px;
}

h4{
	margin:5px 0;
	padding: 0;
	border-bottom: 1px dotted #999;
}
h3{
	font-size: 20px;
	font-weight: bold;
	border-bottom:1px dotted #f0f0f0;
	margin: 10px 0 10px 
}
#header h1{
	font-size:35px;
	color:#fefefe;
	text-transform: uppercase;
	margin: 10px 0 0 0;
	padding: 0;
}

#header h2{
	font-size:20px;
	color:#000;
	text-transform: uppercase;
	margin: 0px 0;
	padding: 0
}

#header h1 a{
	color:#fefefe;
	text-decoration: none;
}

#header h5{
	text-transform: lowercase;
	font-style: italic;
	font-weight: bold;
	font-size:14px;
	text-align: right;

}

/**CONTENT**/
#content{
	background: #f8f8f8;
	background-image: none;
	min-height: 400px;
	padding:10px;
}

.dataTables_length{
	width:49% !important;
	float:left !important;
}
.dataTables_filter{

	float:right !important;
	margin-bottom: 10px;
	margin-top: 0px
}
#table{
	clear: both;
	margin-top: 5px
}
.menu {
	margin-bottom: 0
}
[class*="col_"] {
	margin-top: 0;
	margin-bottom: 0
}
.dataTables_wrapper{
	margin-top: 10px
}
.paging_full_numbers {
	width: 400px;
	height: 22px;
	line-height: 22px;
}

.paging_full_numbers span.paginate_button,
 	.paging_full_numbers span.paginate_active {
	border: 1px solid #f90;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	padding: 2px 5px;
	margin: 0 3px;
	cursor: pointer;
	*cursor: hand;
	color:#fff;
}
.paging_full_numbers span.paginate_button {
	background-color: #d70;
}

.paging_full_numbers span.paginate_button:hover {
	background-color: #f90;
}

.paging_full_numbers span.paginate_active {
	background-color: #a50;
}

table.display tr.even.row_selected td {
	background-color: #B0BED9;
}

table.display tr.odd.row_selected td {
	background-color: #9FAFD1;
}
#table_paginate {
	margin-top:5px;
	width: 100%;
	text-align: right;
}
#table_info{
	text-align: right;
}
</style>
<link rel="stylesheet" href="<?php echo base_url().'asset/themes/base/jquery-ui.css'?>">
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>asset/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="<?php echo base_url()?>asset/media/js/jquery.dataTables.js"></script>
		
		<script src="<?php echo base_url().'asset/ui/jquery.ui.core.js';?>"></script>
		<script src="<?php echo base_url().'asset/ui/jquery.ui.widget.js';?>"></script>
		<script src="<?php echo base_url().'asset/ui/jquery.ui.datepicker.js'?>"></script>

		<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
		
		$(function() {
				$( ".tgl" ).datepicker({
		                dateFormat:"yy-mm-dd",
		                changeMonth: true,
						changeYear: true,
		                showButtonPanel: true
		                });
		       
			});
				$('#table').dataTable({

		"bStateSave": true,
		"aaSorting": [[ 0, "desc" ]],
		"sPaginationType": "full_numbers",
		"sPageButton": "paginate_button",
		"sPageButtonActive": "paginate_active",
		"sPageButtonStaticDisabled": "paginate_button paginate_button_disabled",
		"sPageFirst": "first",
		"sPagePrevious": "previous",
		"sPageNext": "next",
		"sPageLast": "last",
		

				});

			} );
		</script>