<?php if(isset($_POST['mulai_tgl']) AND isset($_POST['sampai_tgl'])){
	echo '<div class="col_12" style="text-align:right;padding:5px;margin:10px 5px;font-size:18px"><a href="'.base_url('admin/laporan').'"><i class="icon-reply"></i> Kembali</a> | <a href="'.base_url('admin/renderlaporan?mulai='.$_POST['mulai_tgl'].'&sampai='.$_POST['sampai_tgl']).'" target="_BLANK"><i class="icon-print" title="Print Bukti Terkirim"></i> Print Laporan</a></div>';
	

	$tmpl = array ( 'table_open'  => '<table id="table">' );
	$this->table->set_heading('No.','Nama','No. Hp/ Telp.','Berat Paket','Jumlah Paket','Tujuan','Tanggal Kirim','Tanggal Sampai','Status');
	
	$i=$tampil->num_rows()+1;

	foreach ($tampil->result() as $row) {

	$sts=$row->status;
	if($sts==0){
		$status='<font color="orange"><i>Menunggu</i></font>';
	}
	elseif($sts==1){

		$status='<font color="green"><i>Terkirim</i></font>';
	}

	if($row->tgl_sampai=="0000-00-00"){
		$tgl_sampai='-';
	}
	else{
		$tgl_sampai=nama_hari($row->tgl_sampai).', '.standar_tanggal($row->tgl_sampai);
	}

	$this->table->add_row($row->id_paket,$row->nama_cos,$row->hp_cos,$row->berat_paket.' kg',$row->jumlah_paket.' Buah',$row->nama_pos,nama_hari($row->tgl_kirim).','.standar_tanggal($row->tgl_kirim),$tgl_sampai,$status);

}

	$this->table->set_template($tmpl);
	echo $this->table->generate();



 }

 else{ ?>

<h4><i class="icon-signal"></i>  Membuat Laporan</h4>
<div class="col_12">
<h3>FORM BUAT LAPORAN</h3>
<?php 

echo form_open('admin/laporan','class="vertical"');
echo form_label('Dari Tanggal :');
echo form_error('mulai_tgl').form_input('mulai_tgl',set_value('mulai_tgl'),'class="tgl" readonly="readonly"');
echo form_label('Sampai Tanggal :');
echo form_error('sampai_tgl').form_input('sampai_tgl',set_value('sampai_tgl'),'class="tgl" readonly="readonly"');
echo form_submit('tampilkan','Tampilkan');

echo form_close()?>
</div>


<?php }?>