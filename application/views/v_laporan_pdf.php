<html>

<head>
	<style type="text/css">
	body{

			font-family: 'helvetica';
			padding:10px;
	}
		#table{
			width:100%;
			border-spacing: 0px;
			font-size: 14px

		}
		#table th{
			background: #f82;
			padding:5px 5px;
			color: #fefefe;
			font-size: 16px;
			border:1px solid #999;
		}
		#table tr:nth-child(odd){
			background:#ddd;

		}
		#table tr:nth-child(even){
			background:#f0f0f0;

		}
		#table td{
			border:1px solid #999;
			padding:5px 3px;

		}
		h3{
			padding:0;
			margin: 4px 0
		}
	</style>
</head>

<body>

<img src="<?php echo base_url()?>asset/images/he.png" style="float:left;width:70px;margin-right:10px">
<h3> PT. POS INDONESIA BANJARMASIN (PERSERO)</h3>
report tanggal : <?php echo nama_hari($_GET['mulai']).', '.standar_tanggal($_GET['mulai']).' - '. nama_hari($_GET['sampai']).', '. standar_tanggal($_GET['sampai']);?>
<hr>
<script type="text/php">

		if(isset($pdf)){
			$w=$pdf->get_width();
			$h=$pdf->get_height();

		  	$font=Font_Metrics::get_font("Arial","bold");
		 	$pdf->page_text($w-90,$h-24,"Halaman : {PAGE_NUM} / {PAGE_COUNT}",$font,10,array(0,0,0));
		 	$pdf->page_text(16,$h-24,"Disimpan pada : ".nama_hari(date('Y-m-d')).', '.standar_tanggal(date('Y-m-d')),$font,10,array(0,0,0));
		  	$footer=$pdf->open_object();
			$pdf->line(16,$h-34,$w-16,$h-34,$color,1);
		  	$pdf->close_object();

		  	$pdf->add_object($footer,"all");
		  }
		</script>
<?php
	if($tampil->num_rows()>0){
	$tmpl = array ( 'table_open'  => '<table id="table">' );
	$this->table->set_heading('No.','Nama','Alamat','No. Hp/ Telp.','Berat Paket','Jumlah Paket','Tujuan','Tanggal Kirim','Tanggal Sampai');
	
	$i=$tampil->num_rows()+1;

	foreach ($tampil->result() as $row) {
	if($row->tgl_sampai=="0000-00-00"){
		$tgl_sampai='-';
	}
	else{
		$tgl_sampai=nama_hari($row->tgl_sampai).', '.standar_tanggal($row->tgl_sampai);
	}

	$this->table->add_row('<center>'.$row->id_paket.'</center>',$row->nama_cos,$row->alamat_cos,$row->hp_cos,$row->berat_paket.' kg',$row->jumlah_paket.' Buah',$row->nama_pos,nama_hari($row->tgl_kirim).','.standar_tanggal($row->tgl_kirim),$tgl_sampai);

}

	$this->table->set_template($tmpl);
	echo $this->table->generate();
}
	else{
		echo '<center>Tidak Ada Data Yang Dapat Digunakan !!</center>';
	}
?>
</body>
</html>

