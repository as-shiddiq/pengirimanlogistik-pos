<!-- Menu Horizontal -->

<?php
	$get=$this->uri->segment(2);

	$cur1='';
	$cur2='';
	$cur3='';
	$cur4='';
	if($get==''){
		$cur1='id="current-menu"';
	}
	elseif($get=='pelanggan' || $get=='akun'){
		$cur2='id="current-menu"';
	}
	elseif($get=='paket'){
		$cur3='id="current-menu"';
	}

	elseif($get=='laporan'){
		$cur4='id="current-menu"';
	}

?>
<div class="menuu">
<?php 
		if($this->session->userdata('admin')){
			
		$q="select * from pos where id_user='".$this->session->userdata("admin")."'";
		$val='<i class="icon-user"></i> admin';
			
		}
		elseif($this->session->userdata('user')){
			
		$q="select * from pos where id_user='".$this->session->userdata("user")."'";
		$val='<i class="icon-group"></i> user';
		}

		$row=$this->db->query($q)->row();


	?>
	<h5><?php echo $val.' : '.$row->nama_pos ?> | <?php echo $row->alamat_pos ?></h5>
<ul class="menu">

<!--MENU BERANDA-->
<li  <?php echo $cur1 ?> ><a href="<?php echo base_url()?>"><i class="icon-home"></i> Beranda</a></li>
<?php 

	if($this->session->userdata('admin')){
?>

<!--MENU MASTER ADMIN-->
<li  <?php echo $cur2?>><a><i class="icon-cogs"></i>Master</a>
	<ul>
		<li><a href="<?php echo base_url().$this->uri->segment(1)?>/pelanggan"><i class="icon-group"></i> Pelanggan</a></li>
		<li><a href="<?php echo base_url().$this->uri->segment(1)?>/akun"><i class="icon-user"></i> Akun</a></li>
	</ul>
</li>

<?php }?>
<?php 

	if($this->session->userdata('admin')){
?>

<!--MENU PAKET BARANG ADMIN-->
<li <?php echo $cur3?>><a href="<?php echo base_url().$this->uri->segment(1)?>/paket"><i class="icon-gift"></i> Barang Logistik</a></li>
<li <?php echo $cur4?>><a href="<?php echo base_url().$this->uri->segment(1)?>/laporan"><i class="icon-gift"></i> Laporan</a></li>
<?php }?>

<?php 

	if($this->session->userdata('user')){
?>

<!--MENU PAKET BARANG USER-->
<li <?php echo $cur3?>><a><i class="icon-gift"></i> Barang Logistik</a>
<ul>
<?php
	$q="SELECT * from paket where status=0 and id_user='".$this->session->userdata('user')."'";
	$jumlah=$this->db->query($q)->num_rows();
	if($jumlah==0){
		$jumlah='';
	}

?>
<li><a href="<?php echo base_url().$this->uri->segment(1)?>/paket/belum"><i class="icon-remove-circle"></i> <font color="#f90"><?php echo $jumlah ?></font> Barang Belum Sampai</a></li>
<li><a href="<?php echo base_url().$this->uri->segment(1)?>/paket/sudah"><i class="icon-ok-circle"></i> Barang Sudah Sampai</a></li>
</ul>
</li>
<?php }?>

<li><a href="<?php echo base_url()?>login/logout"><i class="icon-signout"></i> Keluar</a></li>
</ul>

<div style="clear:both">
</div>
</div>