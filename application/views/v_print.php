<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asset/css/kickstart.css" media="all" />                  <!-- KICKSTART -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asset/css/style.css" media="all" />                          <!-- CUSTOM STYLES -->
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/kickstart.js"></script> 

	<title>Print Konfirmasi</title>

	<style type="text/css">
	@page {
size: A5 portrait;
}
/* channeling Müller-Brockmann */
@page :left {
margin: 15mm 10mm 30mm 20mm;
}
@page :right {
margin: 15mm 20mm 30mm 10mm;
}
	@media print and (width: 210mm) and (height: 297mm) {
@page {
/* rules for A4 paper */
body{
		background: #fefefe ;
		width:24cm;
		font-size: 12px;
		margin: auto;
		border:1px dashed #000;
		padding: 10px;
		height: 12cm
	}
	#header{
		border-bottom:1px dashed #000;
		padding:10px;
		margin-bottom: 10px
	}
	#header h1{
		font-size: 20px;
		margin:0px 0 0;
		padding: 0;
		font-weight: bold
	}
	#header h2{
		font-size: 15px;
		padding: 0;
		margin: 0;
		font-weight: bold
	}
	h3{
		font-size: 16px;
		padding: 0;
		margin: 0;
		font-weight: bold
	}

	h4{
		font-size: 14px;
		padding: 0;
		margin: 0;
		font-weight: bold
	}
	table tr:nth-child(odd) td{
		background: #f0f0f0
	}


}
}
body{
		background: #fefefe;
		width:24cm;
		font-size: 12px;
		margin: auto;
		border:1px dashed #000;
		padding: 10px;
		height: 12cm
	}
	#header{
		border-bottom:1px dashed #000;
		padding:10px;
		margin-bottom: 10px
	}
	#header h1{
		font-size: 20px;
		margin:0px 0 0;
		padding: 0;
		font-weight: bold
	}
	#header h2{
		font-size: 15px;
		padding: 0;
		margin: 0;
		font-weight: bold
	}
	h3{
		font-size: 16px;
		padding: 0;
		margin: 0;
		font-weight: bold
	}

	h4{
		font-size: 14px;
		padding: 0;
		margin: 0;
		font-weight: bold
	}
	table tr:nth-child(odd) td{
		background: #f0f0f0
	}

	</style>
</style>	
</head>
<body onload="javascript:window.print()">



	<div id="header">

<img src="<?php echo base_url()?>asset/images/he.png" style="float:left;width:70px;margin-right:10px">
		<h1>PT. POS BANJARMASIN (PERSERO)</h1>
		<h2>JL. Lambung Mangkurat NO.19 TLP (0511) 3363745</h2>
	</div>
	<div id="content">
	<h3>Rincian Pengiriman :</h3>

		<?php $row=$tampil->row()?>
	<div class="col_6">
	<?php
	$this->table->add_row('<h4>Rincian Pelanggan</h4>',':','');
	$this->table->add_row('Nama Pelanggan',':',$row->nama_cos);
	$this->table->add_row('Alamat Pelanggan',':',$row->alamat_cos);
	$this->table->add_row('No. Hp/ Telp.',':',$row->hp_cos);

	$this->table->add_row('<h4>Rincian Paket</h4>',':','');
	$this->table->add_row('Berat Paket',':',$row->berat_paket.' Kg');
	$this->table->add_row('Jumlah Paket',':',$row->jumlah_paket.' Buah');


	echo $this->table->generate();
	$this->table->clear();

	?>
	</div>
	<div class="col_6">
	<?php
	$this->table->add_row('<h4>Rincian Pengiriman</h4>',':','');
	$this->table->add_row('Id Pengiriman',':',$row->id_paket);
	$this->table->add_row('Tanggal Kirim',':',nama_hari($row->tgl_kirim).', '.standar_tanggal($row->tgl_kirim));
	$this->table->add_row('Tanggal Sampai',':',nama_hari($row->tgl_sampai).', '.standar_tanggal($row->tgl_sampai));
	$this->table->add_row('Tujuan',':',$row->nama_pos);
	$this->table->add_row('Wilayah',':',$row->alamat_pos);
	echo $this->table->generate();
	$this->table->clear();

	?>
	</div>

	<div style="clear:both"></div>
	</div>

</body>
</html>