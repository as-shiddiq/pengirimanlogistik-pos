-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 05 Okt 2014 pada 09.53
-- Versi Server: 5.1.37
-- Versi PHP: 5.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `pos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `costumer`
--

CREATE TABLE IF NOT EXISTS `costumer` (
  `id_cos` int(11) NOT NULL AUTO_INCREMENT,
  `nama_cos` varchar(50) NOT NULL,
  `alamat_cos` text NOT NULL,
  `hp_cos` bigint(15) NOT NULL,
  PRIMARY KEY (`id_cos`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data untuk tabel `costumer`
--

INSERT INTO `costumer` (`id_cos`, `nama_cos`, `alamat_cos`, `hp_cos`) VALUES
(1, 'Sule OVJ', 'Jl. mana kah sarah', 2147483647),
(5, 'gazali', 'Jl. Balirejo Pelaihari', 2147483647),
(6, 'mahmudin hahah', 'jalan manas saja asalakan tembus ke rumah sidin', 2147483647),
(7, 'azis gagap', 'Jl. swadaya Ketika sua sua', 2147483647),
(8, 'abdurrahman', 'Jl. Ahmad Yani KM.7', 2147483647),
(9, 'nunung', 'menuju jalan impian', 2147483647),
(10, 'andre taulani', 'kjkjkj', 2147483647),
(11, 'entis sutisna', 'Bandung', 2147483647),
(12, 'reza', 'padang luas', 2147483647),
(13, 'jkjkjkj', 'kjkjkjkj', 2147483647),
(14, 'Sule OVJ', 'skdjksdjskdj', 2147483647),
(15, 'jfdkfdjk', 'jdkfjdskfj', 2147483647),
(16, 'ksjfsjdf', 'kjskdjskdjs', 0),
(17, 'jdhfdjhfaigf dsifgh', 'idhsf ifs fidsf skfds kjfsh if', 12345678910232),
(18, 'djhfidsnifhsd', 'uhsdifdifh', 123456789012345),
(19, 'jsahfdjs fsajhf', 'hdjsfh jfdshfjkh', 98348348394835);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket`
--

CREATE TABLE IF NOT EXISTS `paket` (
  `id_paket` int(11) NOT NULL AUTO_INCREMENT,
  `id_cos` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `berat_paket` int(5) NOT NULL,
  `jumlah_paket` int(5) NOT NULL,
  `tgl_kirim` date NOT NULL,
  `tgl_sampai` date NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_paket`),
  KEY `id_cos` (`id_cos`),
  KEY `id_pos` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data untuk tabel `paket`
--

INSERT INTO `paket` (`id_paket`, `id_cos`, `id_user`, `berat_paket`, `jumlah_paket`, `tgl_kirim`, `tgl_sampai`, `status`) VALUES
(9, 1, 2, 1, 1, '2014-04-18', '2014-04-18', 1),
(12, 7, 2, 9, 3, '2014-04-18', '2014-04-18', 1),
(14, 6, 2, 16, 1, '2014-04-18', '2014-04-18', 1),
(15, 8, 2, 1, 3, '2014-04-18', '2014-04-18', 1),
(16, 1, 4, 1, 1, '2014-04-18', '2014-04-18', 1),
(17, 5, 3, 21212, 121212, '2014-04-18', '2014-04-18', 1),
(18, 1, 4, 1, 9, '2014-04-19', '0000-00-00', 0),
(19, 1, 2, 12, 12, '2014-04-19', '0000-00-00', 0),
(20, 1, 2, 9, 9, '2014-04-19', '0000-00-00', 0),
(21, 1, 2, 1, 1, '2014-04-19', '0000-00-00', 0),
(22, 1, 2, 1, 1, '2014-04-19', '2014-04-19', 1),
(23, 12, 6, 9, 1, '2014-05-06', '2014-05-06', 1),
(24, 1, 4, 90, 9, '2014-07-08', '0000-00-00', 0),
(25, 1, 2, 9, 9, '2014-07-08', '0000-00-00', 0),
(26, 6, 3, 0, 0, '2014-07-08', '0000-00-00', 0),
(27, 11, 3, 10, 10, '2014-08-17', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pos`
--

CREATE TABLE IF NOT EXISTS `pos` (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `level` int(1) NOT NULL,
  `nama_pos` varchar(50) NOT NULL,
  `alamat_pos` text NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `pos`
--

INSERT INTO `pos` (`id_user`, `nama_user`, `password`, `level`, `nama_pos`, `alamat_pos`) VALUES
(1, 'admin', '123456', 1, 'PT.POS INDONESIA BANJARMASIN (persero)', 'Banjarmasin'),
(2, 'pelaihari', '123456', 2, 'PT.POS PELAIHARI', 'jsdkfjdkjfkfdjkdfd'),
(3, 'naruto', '123456', 2, 'PT. POS JALAN SAJA TANPA MUNDUR-MUNDUR', 'Padang Luas'),
(4, 'kintap', '123456', 2, 'POS KINTAP', 'kintap'),
(5, 'dfdfd', 'sdnss', 2, 'PT.POS apa yo lah', 'kati'),
(6, 'aku', '123456', 2, 'akuuuu', 'akuuu');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `paket`
--
ALTER TABLE `paket`
  ADD CONSTRAINT `paket_ibfk_4` FOREIGN KEY (`id_cos`) REFERENCES `costumer` (`id_cos`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paket_ibfk_5` FOREIGN KEY (`id_user`) REFERENCES `pos` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
