<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function generate_pdf($html='',$tgl=''){

	include("dompdf/dompdf_config.inc.php");
  
  $dompdf = new DOMPDF();
  $dompdf->load_html($html);
  $dompdf->set_paper('A4', 'landscape');
  $dompdf->render();
  
  $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
    
}

